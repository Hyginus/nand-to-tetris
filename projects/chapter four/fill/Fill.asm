// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// pseudo code
// (InfiniteLoop){
   // if key is pressed( 
       // goto ONloop
   //) 
   // else goto OFFLoop
//}

// (ONLoop)
    //SCREEN=1
    //goback to InfiniteLoop

// (OFFLoop)
    //SCREEN=0
    // goback to InfiniteLoop

// Put your code here.
    (InfiniteLoop)
    @SCREEN
    D=A
    @addr
    M=D
    @8191
    D=A
    @n
    M=D
    @i
    M=0
    @KBD  // check if the keyboard is pressed
    //A=M
    D=M
    @ONLoop
    D; JGT

    (OFFLoop)
    @addr
    A=M
    M=0
    @i
    M=M+1
    @addr
    M=M+1 // increment the memory address
    @i
    D=M
    @n
    D=D-M
    @OFFLoop
    D; JLE
    @InfiniteLoop
    0; JMP

    (ONLoop)
    @addr
    A=M
    M=-1 // set the fisrt 16pixels to black
    @i
    M=M+1
    @addr
    M=M+1 // increment the memory address
    @i
    D=M
    @n
    D=D-M
    @ONLoop
    D; JLE
    @InfiniteLoop
    0; JMP


