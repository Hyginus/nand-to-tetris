// Drawing a Rectangle of width 16 pixels and
// length that is been determine by the user
// if (i=0, i<n, i++)
    @SCREEN
    D=A
    @addr
    M=D

    @R0
    D=M
    @n
    M=D
    @i
    M=0

    (LOOP)
    @i
    D=M
    @n
    D=D-M
    @End
    D; JGT // if i >n then go to End
    @addr
    A=M
    M=-1
    @i
    M=M+1
    @32
    D=A
    @addr
    M=D+M
    @LOOP
    0; JMP

    (End)
    @End
    0; JMP
