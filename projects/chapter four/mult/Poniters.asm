// Pointers
// Any variable that stores memory address is like arr and i in the case below are called Pointers
// The code below set memory R100 t0 R109 t0 -1
// If (i=0, i<n, ++i){
//    arr[M+i]=-1
// }
// selecting address 100
    @100
    D=A
    @arr
    M=D
//    slecting address 10
    @10
    D=A
    @n
    M=D
//    selecting i to zero
    @i
    M=0

    (Loop)
    @i
    D=M
    @n
    D=D-M
    @End
    D; JEQ
    @arr
    D=M
    @i
    A=D+M
    M=-1
// Incrementing i
    @i
    M=M+1
    @Loop
    0; JMP
// End loop
    (End)
    @End
    0; JMP
