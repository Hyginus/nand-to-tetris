// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
// setting R2 to zero
@R2
M=0

// check if R1 is greater tyhan zero
@R1
D=M
@Step
D; JGT

// if R1 is less than zero then jump to End loop
@End
0; JMP

// accept R0 input and do the multiplication
(Step)
@R2
D=M

@R0
D=D+M

// set the result back to R2
@R2
M=D

// Reduce R1 by 1 and check if it is greater than Zero 
@R1
D=M-1
M=D

// if it is greater than Zero then jump back to Step loop
@Step
D; JGT

(End)
@End
0; JMP
